const router = require ('express').Router();
const User = require ('../models/User');
const bcrypt = require ('bcrypt');

//inscription

router.post("/inscription", async (req,res) =>{


    try {

        const salt = await bcrypt.genSalt(10)
        const newUser = User({
            username: req.body.username,
            email: req.body.email,
            password: await bcrypt.hash(req.body.password, 10),

        });

        const user = await newUser.save();
        res.status(200).json(user)

    }
    catch (err) {
        res.status(500).json(err)
    }


})

module.exports = router